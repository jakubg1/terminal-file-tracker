import keyboard, os
from colorama import Fore, Back, Style
from time import sleep

# Setting up terminal styles
RESET = Style.RESET_ALL
D = Back.CYAN
F = Back.YELLOW
NEW = Back.BLUE
CURRENT = Back.MAGENTA
STANDARD = ""

class DirectoryManager():

    showedDirectories = []
    currentsHistory = {}

    def __init__(self, path=""):
        if path.startswith("/"): self.path = path
        else:
            if path.startswith("."): path = path[1:]
            self.path = os.getcwd() 

        if not self.isDir(): self.path = os.getcwd()

        self.getListOfDirs()
        
        for file in self.files:
            if file not in self.showedDirectories:
                self.showedDirectories.append(file)
        
        self.printDirectories()

        keyboard.on_press_key('left arrow', self.goParent)
        keyboard.on_press_key('right arrow', self.goDeeper)
        keyboard.on_press_key('down arrow', self.goDown)
        keyboard.on_press_key('up arrow', self.goUp)
        keyboard.on_press_key('delete', self.deleteFileOrDir)
        keyboard.on_press_key('l', self.tree)

        self.loop()

    # checking if the current path is directory
    def isDir(self):

        if os.path.isdir(self.path): return True
        
        return False

    # Getting local files
    def getListOfDirs(self):
        # Getting list of dirs
        self.files = list(sorted(os.listdir(self.path)))

        # Setting up current (pointer)
        if self.path in self.currentsHistory.keys(): self.current = self.currentsHistory[self.path]
        elif len(self.files) > 0 : self.current = self.files[0]
        else: self.current = None

    # Marking files as showed
    def updateUnshowedFiles(self):
        for file in self.files:
            if file not in self.showedDirectories: self.showedDirectories.append(file)

    # Saving current to history
    def saveCurrent(self):
        if self.current is not None: self.currentsHistory[self.path] = self.current

    # Looking for changes in dirs
    def refresh(self):
        refreshedFiles = os.listdir(self.path)
        flag = False

        for i, file in enumerate(self.files):
            if file not in refreshedFiles:
                self.files.pop(i)
                flag = True
        
        for file in refreshedFiles:
            if file not in self.files: flag = True

        self.files = sorted(refreshedFiles)

        if flag: self.printDirectories()

    # Printing changes
    def printDirectories(self):
        os.system("clear") 

        if len(self.files) == 0:
            print("No files or directories")

        for file in self.files:
            if os.path.isdir(self.path+"/"+file): print(f"{D}D{RESET}", end="")
            else:print(f"{F}F{RESET}", end = "")

            if self.current == file: print(f"{CURRENT}{file}{RESET}".ljust(50))
            elif file not in self.showedDirectories: print(f"{NEW}{file}{RESET}".ljust(50))
            else: print(f"{STANDARD}{file}{RESET}".ljust(50))

    # Printing the content of file
    def printFile(self, filename):

        os.system("clear")
        os.system(f"cat {filename}")

    # Navigating in terminal
    def goUp(self, event):
        if self.isDir() and len(self.files) > 1:
            sleep(0.01)
            index = self.files.index(self.current)-1

            # Protect from setting invalid index 
            if index < 0: index = len(self.files)-1

            self.current = self.files[index]
            self.printDirectories()

    # Navigating in terminal
    def goDown(self, event):
        if self.isDir() and len(self.files) > 1:
            sleep(0.01)
            index = self.files.index(self.current)+1

            # Protect from setting invalid index 
            if index == len(self.files): index = 0

            self.current = self.files[index]
            self.printDirectories()

    # Moving to the parent dir
    def goParent(self, event):
        sleep(0.01)
        childFolder = self.path[self.path.rindex("/")+1:]
        if self.isDir(): self.saveCurrent()
        self.updateUnshowedFiles()

        self.path = self.path[0:self.path.rindex("/")]
        if self.path == "": self.path = "/"

        self.getListOfDirs()
        self.current = childFolder
        self.updateUnshowedFiles()
        self.printDirectories()

    # Moving to the child dir
    def goDeeper(self, event):
        sleep(0.01)
        self.saveCurrent()
        if len(self.files) > 0:
            if os.path.isdir(self.path + "/" + self.current): 
                self.path = self.path + "/" + self.current
                self.getListOfDirs()
                self.updateUnshowedFiles()
                self.printDirectories()
            elif os.path.isfile(self.path + "/" + self.current):
                self.path = self.path + "/" + self.current
                self.printFile(self.path)

    # Deleting current file or dir
    def deleteFileOrDir(self, event):
        if not os.path.isfile(self.path):
            sleep(0.01)
            os.system("clear")
            i = input(f"Do you want to remove the file {self.path + "/" + self.current}? [Click enter (or 'y', 'Y') to continue]: ")
            if i[-1] in ["y","Y","~"]:
                if os.path.isdir(self.path + "/" + self.current): os.system(f"rm -R {self.path + "/" + self.current}")
                else: os.system(f"rm {self.path + "/" + self.current}")
                if self.path in self.currentsHistory.keys() and self.currentsHistory[self.path] == self.current: self.currentsHistory.pop(self.path)
            self.getListOfDirs()
            self.printDirectories()

    # Getting the current path
    def getPath(self):
        return self.path
        
    # printing tree starting from the current directory
    def tree(self, event):
        if os.path.isdir(self.path + "/" + self.current):
            os.system("clear")
            for file in self.files:
                if os.path.isdir(self.path+"/"+file): print(f"{D}D{RESET}", end="")
                else:print(f"{F}F{RESET}", end = "")

                if self.current == file: 
                    print(f"{CURRENT}{file}{RESET}".ljust(50))
                    os.system(f"tree {self.path + "/" + self.current}")
                elif file not in self.showedDirectories: print(f"{NEW}{file}{RESET}".ljust(50))
                else: print(f"{STANDARD}{file}{RESET}".ljust(50))

    # running the app
    def loop(self):
        while True:
            if self.isDir(): self.refresh()
            sleep(1)
        